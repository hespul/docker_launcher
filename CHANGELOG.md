# Changelog

## 2020.01.25

### Change `__for_every_service_do__` behavior. 

Now if a single service exactly match the given service name, commands are executed for this service.
Else commands are executed for all services matching the pattern.

Function is renamed `__for_one_or_many_services_do__`
