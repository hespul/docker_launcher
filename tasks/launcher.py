# -*- coding: utf-8 -*-
"""
Launcher for docker-compose applications
yml are stored in containers-available and containers-enabled (with symlink and run level)

Author: Alexandre Norman <norman@xael.org>
License: GPLv3

Call with invoke (http://pyinvoke.org/)

Available tasks:

  launcher.disable          Disable service
  launcher.enable           Enable service with start level
  launcher.list_available   List available containers from containers-available directory
  launcher.list_enabled     List enabled containers from containers-enabled directory
  launcher.ps               show ps container
  launcher.restart          Restart docker compose
  launcher.start            Start container
  launcher.stop             Stop container
"""

from invoke import task
import os


def __available_directory__():
    return "./containers-available"


def __enabled_directory__():
    return "./containers-enabled"


def __get_containers_definition__(
    get_directory=__enabled_directory__, reverse_sort=False
):
    """
    Search in containers-enabled directory for yml files and order them by name

    get_directory -- function which return directory to look inside
    reverse_sort -- reverse sort of containers names if true
    """

    containers = []
    for file in os.listdir(get_directory()):
        if file.endswith(".yml"):
            containers.append(file[:-4])

    if not reverse_sort:
        containers.sort()
    else:
        containers.sort()
        containers.reverse()

    return containers


def run_command(tsk, command, service=None, pty=True, echo=True):
    """
    Run docker compose command for the target
    """
    return tsk.run(
        "{docker_compose} -f {dcfile} {project_name} {command}".format(
            docker_compose=tsk["docker_compose"],
            dcfile="containers-available/{}.yml".format(service),
            command=command,
            project_name="-p {}".format(service),
        ),
        pty=True,
        # echo=True,
    )


def __up__(tsk, service=None, level=None, container=""):
    """
    Start docker compose
    """
    run_command(tsk, command="up -d {}".format(container), service=service)
    return


def __down__(tsk, service=None, level=None, container=""):
    """
    Stop docker compose
    """
    if container:
        run_command(tsk, command="stop {}".format(container), service=service)
        run_command(tsk, command="rm -f {}".format(container), service=service)
    else:
        run_command(tsk, command="down", service=service)

    return


def __ps__(tsk, service=None, level=None):
    """
    Show ps docker compose
    """
    run_command(tsk, command="ps", service=service)
    return


def __exec__(tsk, service=None, level=None, container=None, command=None):
    run_command(tsk, service=service, command="exec {} {}".format(container, command))
    return


@task(help={"service": "Service to restart or all if empty"})
def restart(tsk, service=None, level=None, container=""):
    """
    Restart docker compose
    """
    stop(tsk, service=service, container=container)
    start(tsk, service=service, container=container)
    return


@task(
    help={
        "service": "Service to launch or all if empty",
        "container": "Container to launch or all if empty",
    }
)
def start(tsk, service=None, container=""):
    """
    Start container
    """
    __for_one_or_many_services_do__(
        tsk,
        service,
        call=__up__,
        empty_message="No Service to start",
        call_message="Starting {}",
        params={"container": container},
    )


@task(
    help={
        "service": "Service to stop or all if empty",
        "container": "Container to stop (stop+rm) or all(down) if empty",
    }
)
def stop(tsk, service=None, container=""):
    """
    Stop container
    """
    __for_one_or_many_services_do__(
        tsk,
        service,
        call=__down__,
        empty_message="No Service to stop",
        call_message="Stoping {}",
        reverse_sort=True,
        params={"container": container},
    )


@task(
    help={
        "service": "Service to exec command on",
        "container": "Container of service to exec command on",
        "args": "Command to exec on container's service",
    }
)
def execute(tsk, service=None, container=None, args=None):
    """
    Run command inside container using docker-compose exec
    """
    if not service:
        print("Please provide a service")
        return
    if not container:
        print("Please provide a container")
        return
    if not args:
        print("Please provide args")
        return

    __for_one_or_many_services_do__(
        tsk,
        service,
        call=__exec__,
        empty_message="No service to exec something on",
        call_message="Exec on {}",
        reverse_sort=True,
        params={"command": args, "container": container},
    )


@task(help={"service": "Service to ps"})
def ps(tsk, service=None):
    """
    show ps container
    """
    __for_one_or_many_services_do__(
        tsk,
        service,
        call=__ps__,
        empty_message="No Service to query",
        call_message="Query {}",
    )


def __for_one_or_many_services_do__(
    tsk,
    service,
    call,
    empty_message=None,
    call_message=None,
    reverse_sort=False,
    get_directory=__enabled_directory__,
    params={},
):
    """
    For each [service] exec [call] after printing [call_message] or [empty_message] if no service
    """
    if service is None:
        services = __get_containers_definition__(
            reverse_sort=reverse_sort, get_directory=get_directory
        )

    else:
        all_services = __get_containers_definition__(
            reverse_sort=reverse_sort, get_directory=get_directory
        )

        one_service = [s for s in all_services if service == s[3:]]

        if len(one_service) == 1:
            print(">>> Match exactly one service : ", one_service[0][3:])
            services = one_service
        else:
            services = [s for s in all_services if service in s[3:]]
            print(">>> Match many services : ", [x[3:] for x in services])

    if len(services) == 0:
        if empty_message:
            print(empty_message)
        return

    for lvlsrv in services:
        level, s = __split_level_and_service__(lvlsrv)
        if call_message:
            print(call_message.format(s))

        try:
            call(tsk=tsk, service=s, level=level, **params)
        except:
            print("ERROR:", call_message.format(s))

    return


def __split_level_and_service__(service):
    """
    Returns (level, service)
    """
    return (service.split("-")[0], "-".join(service.split("-")[1:]))


@task()
def list_available(tsk):
    """
    List available containers from containers-available directory
    """
    for service in __get_containers_definition__(get_directory=__available_directory__):
        print(service)


@task()
def list_enabled(tsk):
    """
    List enabled containers from containers-enabled directory
    """
    for service in __get_containers_definition__():
        print(service)


@task(
    help={
        "service": "Service to enable",
        "level": "start order (00 [first] to 99 [last])",
    }
)
def enable(tsk, service, level):
    """
    Enable service with start level
    """
    available = __get_containers_definition__(get_directory=__available_directory__)
    enabled = __get_containers_definition__()

    if service in available:
        try:
            level = int(level)
            assert level <= 99
            assert level >= 0
        except:
            print("Error: level is not in range 00-99")

        for s in enabled:
            if service == __split_level_and_service__(s)[1]:
                print("Service already enabled at level {}".format(s[:2]))
                return

        os.symlink(
            "../containers-available/{}.yml".format(service),
            "containers-enabled/{:02}-{}.yml".format(level, service),
        )
    else:
        print(
            'Error: service "{}" not in containers-available {}'.format(
                service, available
            )
        )


@task(help={"service": "Service to disable"})
def disable(tsk, service):
    """
    Disable service
    """
    enabled = __get_containers_definition__()

    for s in enabled:
        if service == __split_level_and_service__(s)[1]:
            os.remove("containers-enabled/{}.yml".format(s))
            print('Service "{}" is now disabled'.format(service))
            return

    else:
        print('Service "{}" not found'.format(service))
