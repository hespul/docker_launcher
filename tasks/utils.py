# -*- coding: utf-8 -*-
from invoke import task
import os

from .launcher import __for_one_or_many_services_do__, run_command


def __build__(tsk, service=None, level=None):
    """
    Build from docker compose
    """
    run_command(tsk, command="build", service=service)
    return


@task(help={"service": "Service to build or all if empty"})
def build(tsk, service=None):
    """
    Build service
    """
    __for_one_or_many_services_do__(
        tsk,
        service,
        call=__build__,
        empty_message="No Service to build",
        call_message="Building {}",
    )


@task(help={"service": "Service to pull"})
def pull(tsk, service, level=None):
    """
    Pull docker compose
    """
    run_command(tsk, command="pull", service=service)
    return
