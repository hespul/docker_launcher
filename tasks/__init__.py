from invoke import Collection
import os
import socket
from tasks import utils, launcher, backups, frontend

ns = Collection(
    disable=launcher.disable,
    enable=launcher.enable,
    ls=launcher.list_available,
    list_available=launcher.list_available,
    list_enabled=launcher.list_enabled,
    ps=launcher.ps,
    restart=launcher.restart,
    start=launcher.start,
    stop=launcher.stop,
    exec=launcher.execute,
    utils=utils,
    backups=backups,
    frontend=frontend,
)

try:
    import my_tasks  # noqa

    ns.add_collection(my_tasks, "my")
except ImportError:
    pass


# def __get_server():
#     hostname = socket.gethostname().split('.')[0]
#     if os.path.exists('compose/envs-{}'.format(hostname)):
#         return hostname
#     return None

ns.configure({"docker_compose": "/usr/local/bin/docker-compose"})

# ns.configure({
#     'target': 'ctxdev',
#     'registry': 'registry.coutosuix.fr:5000',
#     'server': __get_server(),
#     'all_servers': ['grandelame.coutosuix.fr', 'coupeongle.coutosuix.fr', 'staging.coutosuix.fr', ]
# })
