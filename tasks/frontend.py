# -*- coding: utf-8 -*-
"""
Frontend helper for docker-compose applications
yml are stored in containers-available and containers-enabled (with symlink and run level)

Author: Alexandre Norman <norman@xael.org>
License: GPLv3

Call with invoke (http://pyinvoke.org/)

Available tasks:

  frontend.reload_nginx         Reload nginx configuration
  frontend.renew_certificates   Renew Let's encrypt certificates
"""

import os

from invoke import task
from .launcher import run_command


def __build__(tsk, service=None, level=None):
    """
    Build from docker compose
    """


@task(help={"service": "Reload nginx configuration"})
def reload_nginx(tsk, service="frontend"):
    """
    Reload nginx configuration
    """
    run_command(tsk, command="exec nginx nginx -s reload", service=service)
    return


@task(help={"service": "Reload nginx configuration"})
def renew_certificates(tsk, service="frontend"):
    """
    Renew Let's encrypt certificates
    """
    os.system("certbot renew")
    reload_nginx(tsk, service)
    return
