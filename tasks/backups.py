# -*- coding: utf-8 -*-
from invoke import task
import os
from functools import partial

from .launcher import (
    __for_one_or_many_services_do__,
    run_command,
    __get_containers_definition__,
    __available_directory__,
    __split_level_and_service__,
)


def __backupable_directory__():
    return "./containers-backupable"


def __backup_cmd__(tsk, service=None, level=None, command="backup"):
    """
    Launch backup command
    """
    database = level
    run_command(
        tsk, command="run --rm {} {}".format(database, command), service=service
    )
    return


def __backup__(tsk, service=None, level=None, name=""):
    __backup_cmd__(tsk, service=service, level=level, command="backup {}".format(name))


def __list_backup__(tsk, service=None, level=None):
    __backup_cmd__(tsk, service=service, level=level, command="list-backups")


def __restore_backup__(tsk, backupfile, service=None, level=None):
    if service is None:
        raise ValueError("You must specify a service")
    __backup_cmd__(
        tsk, service=service, level=level, command="restore {}".format(backupfile)
    )


@task(help={"service": "Service to backup or all if empty"})
def do(tsk, service=None, name=""):
    """
    Do database backup
    """
    __for_one_or_many_services_do__(
        tsk,
        service=service,
        call=partial(__backup__, name=name),
        empty_message="No Service to backup",
        call_message="Backuping {}",
        get_directory=__backupable_directory__,
    )


@task()
def list_backupable(tsk):
    """
    List backupable containers from containers-backupable directory
    """
    for service in __get_containers_definition__(
        get_directory=__backupable_directory__
    ):
        print(service)


@task(
    help={"service": "Service to enable", "container": "container name of the database"}
)
def enable(tsk, service, container):
    """
    Enable service database backup for container
    """
    available = __get_containers_definition__(get_directory=__available_directory__)
    enabled = __get_containers_definition__(get_directory=__backupable_directory__)

    if service in available:

        for s in enabled:
            if (
                service == __split_level_and_service__(s)[1]
                and container == __split_level_and_service__(s)[0]
            ):
                print(
                    "Database backup for service already enabled for container {}".format(
                        __split_level_and_service__(s)[0]
                    )
                )
                return

        os.symlink(
            "../containers-available/{}.yml".format(service),
            "containers-backupable/{}-{}.yml".format(container, service),
        )
    else:
        print(
            'Error: service "{}" not in containers-available {}'.format(
                service, available
            )
        )


@task(
    help={
        "service": "Service to disable",
        "container": "container name of the database",
    }
)
def disable(tsk, service, container):
    """
    Disable service database backup for container
    """
    enabled = __get_containers_definition__(get_directory=__backupable_directory__)

    for s in enabled:
        if (
            service == __split_level_and_service__(s)[1]
            and container == __split_level_and_service__(s)[0]
        ):
            os.remove("containers-backupable/{}.yml".format(s))
            print('Database backup for service "{}" is now disabled'.format(service))
            return

    else:
        print('Service "{}" not found'.format(service))


@task(help={"service": "Service to backup or all if empty"})
def list(tsk, service=None):
    """
    List database backup
    """
    __for_one_or_many_services_do__(
        tsk,
        service,
        call=__list_backup__,
        empty_message="No Service to list backup for",
        call_message="\nListing backups for {}",
        get_directory=__backupable_directory__,
    )


@task(
    help={
        "service": "Service to restore",
        "backupfile": "Backup file to restore",
        "container": "container name of the database",
    }
)
def restore(tsk, service, container, backupfile):
    """
    List database backup
    """
    __restore_backup__(tsk, backupfile, service=service, level=container)
