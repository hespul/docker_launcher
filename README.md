# docker_launcher

## Purpose

Docker Launcher is a start / stop / enable / disable script to handle launch of
docker containers by using docker-compose files. A sort of orchestrator for one
server without bell and whistle but KISS principle in mind.

Available services have their docker-compose files stored in
`containers-available`. Like Apache enabled containers have links from
`containers-enabled` with a start order defined by a level.

Docker Launcher relies on [Invoke](http://www.pyinvoke.org/), a task execution
service, and [Docker](https://www.docker.com/) (using docker-compose).

## Installation

```shell
mkdir /docker
git clone ssh://git@bitbucket.org/hespul/docker_launcher.git
cd docker_launcher
pip install -r tasks/requirements.txt
```

If you want to add backup in crontab :

```shell
0 20 * * * /bin/bash -c '(cd /docker/docker_launcher && /usr/local/bin/inv backups.do)'
```

### To start at boot 

If you use systemd
```shell
cp docker_launcher.service /etc/systemd/system
systemctl enable docker_launcher.service
```

If you use Init.d

```shell
cp docker_launcher.rcinit /etc/init.d/docker_launcher
update-rc.d docker_launcher defaults
```

## Usage


```shell
$ inv -l
Available tasks:


  disable                       Disable service
  enable                        Enable service with start level
  list-available                List available containers from containers-available directory
  list-enabled                  List enabled containers from containers-enabled directory
  ls                            List available containers from containers-available directory
  ps                            show ps container
  restart                       Restart docker compose
  start                         Start container
  stop                          Stop container  
  backups.disable               Disable service database backup for container
  backups.do                    Do database backup
  backups.enable                Enable service database backup for container
  backups.list                  List database backup
  backups.list-backupable       List backupable containers from containers-backupable directory
  backups.restore               List database backup
  frontend.reload-nginx         Reload nginx configuration
  frontend.renew-certificates   Renew Let's encrypt certificates
  utils.build                   Build service
  utils.pull                    Pull docker compose
```


### Start all services

```shell
inv start
```
Starts all enabled services respecting order defined by level (from 00 to 99).

#### Start one service

```shell
inv start -s SERVICE
```

Starts one specified `SERVICE` which can be available but not enabled by default.

### Stop all services

```shell
inv stop
```
Stops all enabled services respecting order defined by level (from 00 to 99).

#### Stop one service

```shell
inv stop -s SERVICE
```
Stops one specified `SERVICE`.

### List available services

```shell
inv list_available
```

List services defined as available (*.yml files stored in `containers-available` directory.


### List enabled services

```shell
inv list_enabled
```
List services defined as enabled (*.yml files stored in `containers-enabled` directory.
Levels are displayed before service name.
Those service will be started by `inv start`.

### Show running services

```shell
inv ps
```

Launch `docker ps` command to display state of running (or not) containers.


#### Show one running service

```shell
inv ps -s SERVICE
```

Launch `docker ps` command to display state of running (or not) container defined by the `SERVICE` docker-compose file.

### Enable one service

```shell
inv enable -l LEVEL -s SERVICE
```

Enable `SERVICE` with `LEVEL` priority. File from `container-available` is linked to  `container-enabled` with level.

### Disable one service

```shell
inv disable -s SERVICE
```
Removes link from `container-enabled` directory. `SERVICE` will not be started anymore.

### Backups

Backup are defined by .yml files stored in `containers-backupable` directory.
Database type is defined as ms for mysql and pg for postgresql.

#### Enable

```shell
backups.enable -s SERVICE -c CONTAINER
```

Enable service database backup with dbtype container (ms: mysql / pg: postgresql)


#### Disable


```shell
backups.disable -s SERVICE -c CONTAINER
```
Disable backup of service by removing link from `containers-backupable` directory.


#### Do backup

```shell
backups.do -s SERVICE -n before_erase_all
```
Do database backup by launching container backup command.

 * If service omitted, will backup all backupable services
 * name is optional and appended to the backup filename (if backup script support this)


#### List backups

```shell
backups.list -s SERVICE
```
Do database backup by launching container backup command.


#### Restore backup

```shell
backups.restore -s SERVICE -c CONTAINER -b BACKUPFILE
```
Do database backup by launching container backup command.



#### List backupable

```shell
backups.list-backupable
```

List backupable containers from containers-backupable directory


### Build a service

```shell
utils.build -s SERVICE
```
Build service

### Pull a service

```shell
utils.pull -s SERVICE
    ```
Pull docker compose



### Reload nginx configuration

```shell
frontend.reload-nginx
  ```
Reload nginx configuration for frontend without restarting container.


### Renew Let's encrypt certificates
```shell
frontend.renew-certificates
  ```
Use certbot to renew SSL Let's Encrypt certificates
